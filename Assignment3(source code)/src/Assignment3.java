import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import javax.swing.text.AbstractDocument.BranchElement;

public class Assignment3 {

		public static void main(String[] args) {
			
			ArrayList<String> Computer = new ArrayList<String>();
			Computer = CreateIdPc(30);
			Display(Computer);
			DeleteComputer(Computer);
			
		 }
				
		public static ArrayList<String> CreateIdPc(int numPC) {
			ArrayList<String> Computer = new ArrayList<String>();
			int lengthID = 5;
			String ID = "";
			for(int i =0 ; i<numPC ;i++) {
				ID = String.valueOf(i);
				if(ID.length()<lengthID) {
					for(int j = 0 ; j<=lengthID-ID.length();j++) {
						ID = "0"+ID ;
					}
				}
				Computer.add(ID);
			}
			
			return Computer;
		}
		
		public static void Display(ArrayList<String> Computer) {
			
			int idex = 0;
			int newRow = 0;
			System.out.println("Total number of computers Now = " + Computer.size());
			System.out.print("ID Computer : ");
			for (String com : Computer) {
				idex++;
				if(idex == Computer.size()) {
					System.out.print(com);
				}else if(idex != Computer.size()) {
					System.out.print(com + " , ");
				}
				newRow++;
				if(newRow == 5) {
					System.out.print("\n\n              ");
					newRow = 0 ;
				}
				
			}
			
		}
		
		public static Object[] remove(Object[] arr, int index)
		{
		
		if (arr == null || index < 0 || index >= arr.length) {
			return arr;
		}
		
		Object anotherArray[] = new Object[arr.length - 1];
		
		for (int i = 0, k = 0; i < arr.length; i++) {
		
			if (i == index) {
				continue;
			}
			
			anotherArray[k++] = arr[i];
		}
		
		return anotherArray;
		}
		
		public static void DeleteComputer(ArrayList<String> Computer) {
			Scanner sc = new Scanner(System.in);
			Object ComputerArray[] = Computer.toArray();
			boolean DeleteCompleted = false;
			boolean DeleteAgain = false;
			
			while (DeleteCompleted == false) {
				System.out.print("\nInput ID Computer want to delete : ");
				String Input_ID = sc.next();
				for(int i =0 ; i<ComputerArray.length ; i++) {
					if(ComputerArray[i].equals(Input_ID)) {
						System.out.println("\nComputer Delete ID "+ComputerArray[i] + " Completed");
						ComputerArray = remove(ComputerArray , i);
						DeleteCompleted = true;
						break;
					}else {
					}
				}if(DeleteCompleted == false) {
					System.out.println("\nDon't have this ID Try Again");
					System.out.print("Input ID Computer want to delete Again : ");
				}else {
					ArrayList<String> NewComputer = new ArrayList<String>();
					for(Object IdPc:ComputerArray) {
						NewComputer.add(IdPc.toString());
				    }
					System.out.println("Report Update ID Computer");
					Display(NewComputer);
					System.out.println("\nDo you want to delete more?");
					System.out.println("Want press 1");
					System.out.println("No and exit program press anything");
					System.out.print("press : ");
					String WantDelete = sc.next();
					if(WantDelete.equals("1")) {
						DeleteCompleted = false;
					}else{
						DeleteCompleted = true;
						System.out.println("\nexit program Completed");
					}
					
				}
			}
			
			
		}
		

}
